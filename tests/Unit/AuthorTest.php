<?php

namespace Tests\Unit;

use App\Models\Author;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthorTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAuthorsAreUnique()
    {
        $name = 'Milan Kundera';
        $author = Author::create(compact('name'));

        $this->assertNotNull($author->id);

        $this->expectException(\Illuminate\Database\QueryException::class);
        $author = Author::create(compact('name'));
    }
}
