@extends('layouts/app')

@section('content')
<h2>Books</h2>

<div class="text-right mb-3">
    <a href="{{ route('books.create') }}" class="btn btn-primary">
        Add a book
    </a>
</div>

<table class="table table-hover table-responsive-md">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Author</th>
            <th colspan="3">Actions</th>
        </tr>
    </thead>

    <tbody>
    @foreach ($books as $book)
        <tr>
            <td>{{ $book->id }}</td>
            <td>{{ $book->title }}</td>
            <td>
                <a href="{{ route('authors.show', ['id' => $book->author_id]) }}">
                    {{ $book->author->name }}
                </a>
            </td>
            <td>
                <a href="{{ route('books.show', ['id' => $book->id]) }}">
                    Show
                </a>
            </td>
            <td>
                <a href="{{ route('books.edit', ['id' => $book->id]) }}">
                    Edit
                </a>
            </td>
            <td>
                <button
                    type="button"
                    class="btn btn-danger destroy-book"
                    data-id="{{ $book->id }}"
                    data-toggle="modal"
                    data-target="#destroyBookModal"
                >
                    Delete
                </button>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection

@section('js')
<script type="text/javascript">
window.addEventListener('DOMContentLoaded', function(ev) {
    // this is hackish, but it gets the job done
    $('.destroy-book').click(function(ev) {
        var id = $(ev.target).data('id');
        $('#destroyBookModal form')
            .prop('action', "{{ route('books.index') }}/" + id);
    });
});
</script>
@endsection

@include('books/destroy-confirm', [ 'id' => NULL ])
