@section('modals')
@modal([ 'modalId' => 'destroyAuthorModal' ])
    @slot('title') Caution @endslot
    @slot('actions')
        @destroy([ 'url' => route('authors.destroy', compact('id')) ])
        @enddestroy
    @endslot

    <p>
        Are you sure you want to delete the author? This will
        <strong class="text-danger">
            delete all books associated to it
        </strong>.
    </p>
@endmodal
@endsection
