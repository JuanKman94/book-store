<footer class="container text-right">
    Created by
    <a href="https://www.gitlab.com/juankman94">
        JuanKman94
        <img class="social-icon" src="/svg/gitlab.svg" alt="Gitlab">
    </a>
    <a href="https://www.github.com/juankman94">
        <img class="social-icon" src="/images/github.png" alt="Github">
    </a>
</footer>
