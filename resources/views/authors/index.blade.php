@extends('layouts/app')

@section('content')
<h2>Authors</h2>

<div class="text-right mb-3">
    <a href="{{ url('/authors/create') }}" class="btn btn-primary">
        Add an author
    </a>
</div>

<table class="table table-hover table-responsive-md">
    <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th colspan="3">Actions</th>
        </tr>
    </thead>

    <tbody>
    @foreach ($authors as $author)
        <tr>
            <td>{{ $author->id }}</td>
            <td>{{ $author->name }}</td>
            <td>
                <a href="{{ route('authors.show', ['id' => $author->id]) }}">
                    Show
                </a>
            </td>
            <td>
                <a href="{{ route('authors.edit', ['id' => $author->id]) }}">
                    Edit
                </a>
            </td>
            <td>
                <button
                    type="button"
                    class="btn btn-danger destroy-author"
                    data-id="{{ $author->id }}"
                    data-toggle="modal"
                    data-target="#destroyAuthorModal"
                >
                    Delete
                </button>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection

@section('js')
<script type="text/javascript">
window.addEventListener('DOMContentLoaded', function(ev) {
    // this is hackish, but it gets the job done
    $('.destroy-author').click(function(ev) {
        var id = $(ev.target).data('id');
        $('#destroyAuthorModal form')
            .prop('action', "{{ route('authors.index') }}/" + id);
    });
});
</script>
@endsection

@include('authors/destroy-confirm', [ 'id' => NULL ])
