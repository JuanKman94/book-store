# Exercise: Online Book Store

The exercise MUST fulfill the following requirements:

- Use [Laravel](http://laravel.com/) for the backend.
- Any persistence is okay, MySQL, SQLite etc. Just choose the one that feels
most convenient.
- Create a list of books, with the following functions:
  - Add a book to the list.
  - Delete a book from the list.
  - Change an authors name
  - Sort by title or author
  - Search for a book by title or author
  - Export the following in CSV and XML
    - A list with Title and Author
    - A list with only Titles
    - A list with only Authors

The attached image file is an example but feel free to solve the tasks in
whatever manner you think is best and with the layout you consider to be the
best.

![Example UI implementation][example_img]

## Status

- [x] Add a book to the list.
- [x] Delete a book from the list.
- [x] Change an authors name
- [x] Sort by title or author
- [x] Search for a book by title or author
- [x] Export the following in CSV and XML
    1. [x] A list with Title and Author
    2. [x] A list with only Titles
    3. [x] A list with only Authors

## Development

Run the postgres CLI client:

```bash
$ docker-compose exec db psql -U postgres -d yaraku
```

### First Run

```bash
$ docker-compose up -d web
$ docker-compose exec web ./artisan migrate

# Or open, if you're on mac
$ xdg-open http://localhost/
```

## Dependencies

### Back End

* [Ekko](https://github.com/laravelista/Ekko)
* [l5-repository](https://github.com/andersao/l5-repository)

### Front End

* Axios
* Bootstrap 4
* [Open iconic](https://github.com/iconic/open-iconic)
* Vue2
* [Vuetable2](https://ratiw.github.io/vuetable-2)
* [VueSelect](https://github.com/sagalbot/vue-select)

[example_img]: public/images/homework.png
