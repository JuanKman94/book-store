<?php

namespace App\Transformers;

use App\Models\Book;
use League\Fractal\TransformerAbstract;

/**
 * Class BookTransformer.
 *
 * @package namespace App\Transformers;
 */
class BookTransformer extends TransformerAbstract
{
    /**
     * Transform the Book entity.
     *
     * @param \App\Models\Book $model
     *
     * @return array
     */
    public function transform(Book $model)
    {
        return [
            'id'            => (int) $model->id,
            'author_id'     => (int) $model->author_id,
            'title'         => $model->title,
            'author'        => $model->author->name,
            //'created_at'    => $model->created_at,
            //'updated_at'    => $model->updated_at,
        ];
    }
}
