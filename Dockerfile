FROM hitalos/laravel:latest

COPY ./app/ ./app/
COPY ./bootstrap/ ./bootstrap/
COPY ./config/ ./config/
COPY ./database/ ./database/
COPY ./public/ ./public/
COPY ./resources/ ./resources/
COPY ./routes/ ./routes/
COPY ./storage/ ./storage/
COPY ./tests/ ./tests/
COPY ./artisan ./
COPY ./composer.json ./
COPY ./composer.lock ./
COPY ./phpunit.xml ./
COPY ./server.php ./

# install php dependencies
RUN composer install --prefer-dist

# install js dependencies
COPY ./package.json ./
COPY ./package-lock.json ./
COPY ./webpack.mix.js ./

RUN npm install
RUN npm run production
