<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class AuthorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authors = Author::all();
        return view('authors/index', compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->showForm(NULL, 'store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'  => 'required|unique:authors',
        ]);

        try {
            $author = Author::create($data);
            $request->session()->flash('status', Author::CREATED_SUCCESSFULLY);
        } catch (QueryException $ex) {
            $request->session()->flash('error', "Error ocurred: {$ex->getMessage()}");
            return back()->withInput();
        }

        return redirect()->route('authors.show', [ 'id' => $author->id ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $author = Author::with('books')->find($id);

        if ($author == NULL) {
            return redirect()->route('authors.index')
                             ->with('error', Author::NOT_FOUND);
        }

        return view('authors/show', compact('author'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->showForm($id, 'update');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request
            ->validate([
                'name'  => 'required|unique:authors',
            ]);
        $author = Author::find($id);

        if ($author == NULL) {
            return redirect()->route('authors.index')
                             ->with('error', Author::NOT_FOUND);
        }

        try {
            $author->update($data);
            $request->session()->flash('status', Author::UPDATED_SUCCESSFULLY);
        } catch (QueryException $ex) {
            $request->session()->flash('error', "Error ocurred: {$ex->getMessage()}");
            return back()->withInput();
        }

        return redirect()->route('authors.show', [ 'id' => $author->id ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Author::find($id);

        if ($author == NULL) {
            return redirect()->route('authors.index')
                             ->with('error', Author::NOT_FOUND);
        }

        try {
            $author->delete();
        } catch (QueryException $ex) {
            return redirect()->route('authors.index')
                ->with('error', "There was an error deleting the author: {$ex->getMessage()}");
        }

        return redirect()->route('authors.index')
                         ->with('status', Author::DELETED_SUCCESSFULLY);
    }

    /**
     * Display Author form to user.
     *
     * @param  int $id
     * @param  string $action
     * @return \Illuminate\Http\Response
     */
    private function showForm($id = NULL, $action)
    {
        if ($id != NULL) {
            $author = Author::find($id);

            if ($author == NULL) {
                return redirect()->route('authors.index')
                                 ->with('error', Author::NOT_FOUND);
            }
        } else {
            $author = new Author;
        }

        return view('authors/form', compact('author', 'action'));
    }
}
