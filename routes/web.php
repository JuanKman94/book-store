<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    $authors = \App\Models\Author::select('name')->get()->pluck('name');
    return view('home', compact('authors'));
});

Route::resource('authors', 'AuthorsController');
Route::resource('books', 'BooksController');
