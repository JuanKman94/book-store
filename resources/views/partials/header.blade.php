<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-3">
    <a class="navbar-brand" href="/">Book Store</a>

    <button
        class="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#book-navbar"
        aria-controls="book-navbar"
        aria-expanded="false"
        aria-label="Toggle navigation bar"
    >
        <span class="navbar-toggler-icon"></span>
    </button>

    <div id="book-navbar" class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a
                    href="#"
                    class="nav-link dropdown-toggle {{ isActiveRoute('authors.index', 'active') }}"
                    id="authorsDropdown"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    Authors
                </a>
                <div class="dropdown-menu" aria-labelledby="authorsDropdown">
                    <a
                        class="dropdown-item {{ isActiveRoute('authors.index', 'active bg-secondary') }}"
                        href="{{ route('authors.index') }}"
                    >
                        List
                    </a>
                    <a
                        class="dropdown-item {{ isActiveRoute('authors.create', 'active bg-secondary') }}"
                        href="{{ route('authors.create') }}"
                    >
                        New author
                    </a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a
                    href="#"
                    class="nav-link dropdown-toggle {{ isActiveRoute('books.index', 'active') }}"
                    id="booksDropdown"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    Books
                </a>
                <div class="dropdown-menu" aria-labelledby="booksDropdown">
                    <a
                        class="dropdown-item {{ isActiveRoute('books.index', 'active bg-secondary') }}"
                        href="{{ route('books.index') }}"
                    >
                        List
                    </a>
                    <a
                        class="dropdown-item {{ isActiveRoute('books.create', 'active bg-secondary') }}"
                        href="{{ route('books.create') }}"
                    >
                        New book
                    </a>
                </div>
            </li>
        </ul>
    </div>
</nav>
