<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BookRepository.
 *
 * @package namespace App\Repositories\Author;
 */
interface BookRepository extends RepositoryInterface
{
    //
}
