<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    const ALREADY_EXISTS = 'Book already exists.';
    const CREATED_SUCCESSFULLY = 'Book created successfully.';
    const UPDATED_SUCCESSFULLY = 'Book updated successfully.';
    const DELETED_SUCCESSFULLY = 'Book deleted successfully.';
    const NOT_FOUND = 'Book not found.';

    /**
     * Mass-assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'author_id',
        'title',
    ];

    /**
     * Author of the book.
     */
    public function author()
    {
        return $this->belongsTo(Author::class);
    }
}
