<?php

use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Seeder;

class BooksSeeder extends Seeder
{
    private $records = [
        'Leonora Carrington'    => [
            'Down Below',
            'The Hearing Trumpet',
            'The Stone Door',
            'The House of Fear',
        ],
        'Friedrich Nietzsche'   => [
            'The Birth of Tragedy',
            'Thus Spoke Zarathustra',
            'Ecce Homo',
        ],
        'Emily Bronte'          => [
            'Wuthering Heights',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->records as $author => $books) {
            $author = Author::firstOrCreate([ 'name' => $author ]);

            foreach ($books as $book) {
                $attrs = [
                    'title'     => $book,
                    'author_id' => $author->id,
                ];
                Book::firstOrCreate($attrs, $attrs);
            }
        }
    }
}
