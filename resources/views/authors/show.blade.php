@extends('layouts/app')

@section('content')
<h2>{{ $author->name }}</h2>

<div class="text-right">
    <a class="btn btn-primary" href="{{ route('books.create', ['author_id' => $author->id]) }}">
        Add book
    </a>
    <a class="btn btn-primary" href="{{ route('authors.edit', ['id' => $author->id]) }}">
        Edit
    </a>
    <button
        type="button"
        class="btn btn-danger"
        data-toggle="modal"
        data-target="#destroyAuthorModal"
    >
        Delete
    </button>
</div>

<div>
    Lorem aliquid incidunt eaque qui laboriosam! Et optio provident dicta minima tempore at hic maxime Repellendus laudantium laudantium tenetur et quo Ratione quam porro animi quas eaque? Non necessitatibus officia?
</div>

<hr>

<h3>Books</h3>

<section class="author-books row justify-content-around">
@forelse ($author->books as $book)
    <article class="card col-12 col-md-4  m-2">
        <div class="card-body">
            <h4 class="card-title">{{ $book->title }}</h4>

            <p class="card-text">Adipisicing sapiente ipsum maiores enim quibusdam atque debitis. Placeat et sunt voluptates quia quaerat commodi? Delectus fugiat architecto vero nam odio excepturi, amet. Sit aliquam libero autem quas voluptatem eum</p>

            <a class="btn btn-primary" href="{{ route('books.show', ['id' => $book->id]) }}">
                See
            </a>
        </div>
    </article>
@empty
    <p class="col">This author has no books</p>
@endforelse
</section>
@endsection

@include('authors/destroy-confirm', [ 'id' => $author->id ])
