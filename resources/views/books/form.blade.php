@extends('layouts/app')

@section('content')
<h2>
    {{ $action == 'update' ? 'Edit' : 'Add' }} Book
</h2>

@if ($action == 'update')
<div class="text-right">
    <a class="btn btn-primary" href="{{ route('books.show', ['id' => $book->id]) }}">
        Show
    </a>
</div>
@endif

<form action="{{ route("books.{$action}", ['id' => $book->id]) }}" method="POST">
    @csrf
    @if ($action != 'store')
        @method('PUT')
    @endif

    <div class="form-row">
        <div class="col form-group">
            <label for="author_id">
                Author
            </label>
            <select class="form-control" name="author_id" id="author_id" required>
                <option disabled selected>Select author</option>
            @foreach ($authors as $author)
                <option
                    value="{{ $author->id }}"
                    @if ($author->id == $author_id)
                    selected
                    @endif
                >{{ $author->name }}</option>
            @endforeach
            </select>
        </div>

        <div class="col form-group">
            <label for="title">
                Name
            </label>
            <input name="title" id="title" class="form-control" value="{{ $book->title }}" required>
        </div>
    </div>

    @submit([ 'action' => ($action == 'update' ? 'Update' : 'Create') ])
    @endsubmit
</form>
@endsection
