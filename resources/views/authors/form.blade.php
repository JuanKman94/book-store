@extends('layouts/app')

@section('content')
<h2>
    {{ $action == 'update' ? 'Edit' : 'Add' }} Author
</h2>

@if ($action == 'update')
<div class="text-right">
    <a class="btn btn-primary" href="{{ route('authors.show', ['id' => $author->id]) }}">
        Show
    </a>
</div>
@endif

<form action="{{ route("authors.{$action}", ['id' => $author->id]) }}" method="POST">
    @csrf
    @if ($action != 'store')
        @method('PUT')
    @endif

    <div class="form-group">
        <label for="name">
            Name
        </label>
        <input
            name="name"
            id="name"
            class="form-control"
            value="{{ $author->name }}"
            required
        >
    </div>

    @submit([ 'action' => ($action == 'update' ? 'Update' : 'Create') ])
    @endsubmit
</form>
@endsection
