
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// this includes jQuery, bootstrap, axios and the CSRF token
require('./bootstrap');

window.Vue = require('vue');
import NewBook from './components/NewBook.vue';
import BooksVuetable from './components/BooksVuetable.vue';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('new-book', NewBook);
Vue.component('books-vuetable', BooksVuetable);

const app = new Vue({
    el: '#app',
    data() {
        return {
            selectedBook: null,
        };
    },

    methods: {
        /**
         * Fire `delete-book` event. Currently this mathod is called from
         * the confirmation modal.
         *
         * @return {void}
         */
        deleteBook() {
            this.$events.fire('delete-book', this.selectedBook);
        },
    },

    events: {
        /**
         * This methods assumes there's an existing modal with
         * id `destroyBookModal`
         *
         * @param  {Object} Book object
         * @return {void}
         */
        'confirm-delete-book'(book) {
            this.selectedBook = book;
            $('#destroyBookModal').modal('show');
        },
    },
});
