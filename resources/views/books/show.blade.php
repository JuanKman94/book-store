@extends('layouts/app')

@section('content')
<h2>{{ $book->title }}</h2>
<h3 class="text-secondary">
    by <a href="{{ route('authors.show', ['id' => $book->author_id]) }}">
        {{ $book->author->name }}
    </a>
</h3>

<div class="text-right">
    <a class="btn btn-primary" href="{{ route('authors.edit', ['id' => $book->author_id]) }}">
        Edit author
    </a>
    <a class="btn btn-primary" href="{{ route('books.edit', ['id' => $book->id]) }}">
        Edit
    </a>
    <button
        type="button"
        class="btn btn-danger"
        data-toggle="modal"
        data-target="#destroyBookModal"
    >
        Delete
    </button>
</div>

<div>
    Lorem aliquid incidunt eaque qui laboriosam! Et optio provident dicta minima tempore at hic maxime Repellendus laudantium laudantium tenetur et quo Ratione quam porro animi quas eaque? Non necessitatibus officia?
</div>
@endsection

@include('books/destroy-confirm', [ 'id' => $book->id ])
