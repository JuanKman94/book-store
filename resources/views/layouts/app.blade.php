<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Book Store | Yaraku</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
    @include('partials/header')

    <section id='app' class="container">
        <aside>
            @if (session('status'))
                <p class="alert alert-success">{{ session('status') }}</p>
            @elseif (session('error'))
                <p class="alert alert-danger">{{ session('error') }}</p>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </aside>

        @yield('content')
    </section>

    @yield('modals')

    <hr>

    @include('partials/footer')

    <script src="/js/manifest.js"></script>
    <script src="/js/vendor.js"></script>
    <script src="/js/app.js"></script>
@yield('js')
</body>
</html>
