@extends('layouts/app')

@section('content')
<new-book class="mb-3" :authors='{!! $authors !!}'></new-book>

<hr>

<books-vuetable></books-vuetable>

@modal([ 'modalId' => 'destroyBookModal' ])
    @slot('title') Caution @endslot

    <p>
        Are you sure you want to delete the book?
    </p>

    @slot('actions')
        <button @click="deleteBook" class="btn btn-danger text-white">
            Delete
        </button>
    @endslot
@endmodal
@endsection
