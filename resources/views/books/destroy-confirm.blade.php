@section('modals')
@modal([ 'modalId' => 'destroyBookModal' ])
    @slot('title') Caution @endslot

    <p>
        Are you sure you want to delete the book?
    </p>

    @slot('actions')
        @destroy([ 'url' => route('books.destroy', compact('id')) ])
        @enddestroy
    @endslot
@endmodal
@endsection
