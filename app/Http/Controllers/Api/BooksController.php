<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\Book;
use App\Repositories\BookRepository;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BooksController extends Controller
{
    private $books;

    /**
     * Initialize instance with dependencies.
     */
    public function __construct(BookRepository $books)
    {
        $this->books = $books;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = $this->books->with([ 'author', ])->all();
        return $books;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = Response::HTTP_METHOD_NOT_ALLOWED;
        return response()
            ->json([ 'error' => Response::$statusTexts[$status] ], $status);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request
            ->validate([
                'title'     => 'required',
                'author'    => 'required',
            ]);

        // find or create author
        $author = Author::firstOrCreate(
            [ 'name'    => $data['author'], ],
            [ 'name'    => $data['author'], ]
        );

        // create book
        try {
            $book = $this->books
                 ->create([
                     'title'        => $data['title'],
                     'author_id'    => $author->id,
                 ]);
        } catch (QueryException $ex) {
            $error = $ex->getMessage();

            // unique constraint error -- TODO: consider using an error handler
            if ($ex->getCode() == 23505) {
                $error = Book::ALREADY_EXISTS;
            }

            return response()
                ->json(compact('error'), Response::HTTP_BAD_REQUEST);
        } catch (Exception $ex) {
            // TODO: log error

            return response()
                ->json([ 'error' => $ex->getMessage(), ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $book;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = Response::HTTP_METHOD_NOT_ALLOWED;
        return response()
            ->json([ 'error' => Response::$statusTexts[$status] ], $status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Response::HTTP_METHOD_NOT_ALLOWED;
        return response()
            ->json([ 'error' => Response::$statusTexts[$status] ], $status);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = Response::HTTP_METHOD_NOT_ALLOWED;
        return response()
            ->json([ 'error' => Response::$statusTexts[$status] ], $status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = Response::HTTP_METHOD_NOT_ALLOWED;
        return response()
            ->json([ 'error' => Response::$statusTexts[$status] ], $status);
    }

    /**
     * Export books in the specified format.
     *
     * This method relies on the fact that the Book transformer sets the
     * author's name in the `author` property.
     *
     * @see App\Transformers\BookTransformer
     * @param  string $format { csv, xml }
     * @return \Illuminate\Http\Response
     */
    public function export($format)
    {
        if (!in_array($format, [ 'csv', 'xml', ])) {
            $status = Response::HTTP_METHOD_NOT_ALLOWED;
            return response()
                ->json([ 'error' => Response::$statusTexts[$status] ], $status);
        }

        $books = $this->books->with([ 'author', ])->all();
        $fields = config('app.exportFields');

        // remove field if set to 0
        foreach ($fields as $f) {
            if (!request()->query($f, FALSE)) {
                unset($fields[array_search($f, $fields)]);
            }
        }

        // set default fields if empty
        if (count($fields) == 0) {
            $fields = config('app.exportFields');
        }

        // remove not exportable fields from model collection
        $records = array_map(function($author) use ($fields) {
            foreach ($author as $prop => $val) {
                foreach (array_keys($author) as $k) {
                    if (!in_array($k, $fields)) {
                        unset($author[$k]);
                    }
                }

                return $author;
            }
        }, $books['data']);

        if ($format == 'csv') {
            return response()->csv($records);
        } else {
            return response()
                ->xml(
                    $records,
                    Response::HTTP_OK,
                    [
                        'template' => '<books></books>',
                        'rowName' => 'book',
                    ]
                );
        }
    }
}
