<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    const CREATED_SUCCESSFULLY = 'Author created successfully.';
    const UPDATED_SUCCESSFULLY = 'Author updated successfully.';
    const DELETED_SUCCESSFULLY = 'Author deleted successfully.';
    const NOT_FOUND = 'Author not found.';

    /**
     * Mass-assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Books written by the author.
     */
    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
