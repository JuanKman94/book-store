<?php

namespace Tests\Unit;

use App\Models\Author;
use App\Models\Book;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test the book records are deleted when the author is deleted.
     *
     * @return void
     */
    public function testItGetsCascadeDeleted()
    {
        $n_books = 3;
        $author = factory(Author::class)->create();
        $books = factory(Book::class, $n_books)
            ->create([ 'author_id' => $author->id, ]);

        $this->assertEquals(1, Author::count());
        $this->assertEquals($n_books, Book::count());

        $author->delete();

        $this->assertEquals(0, Author::count());
        $this->assertEquals(0, Book::count());
    }

    /**
     * Test the book records are unique by author and title.
     *
     * @return void
     */
    public function testBooksAreUnique()
    {
        $authors = factory(Author::class, 2)->create();
        $title = 'A book';

        $book1 = Book::create([
            'title' => $title,
            'author_id' => $authors[0]->id
        ]);

        $this->assertNotNull($book1->id);

        $book2 = Book::create([
            'title' => $title,
            'author_id' => $authors[1]->id
        ]);

        $this->assertNotNull($book1->id);

        $this->expectException(\Illuminate\Database\QueryException::class);
        Book::create([
            'title' => $title,
            'author_id' => $authors[0]->id
        ]);
    }
}
