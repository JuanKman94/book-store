<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::with('author:id,name')->get();
        return view('books/index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->showForm(NULL, 'store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'author_id'  => 'required',
            'title'  => 'required',
        ]);

        try {
            $book = Book::create($data);
            $request->session()->flash('status', Book::CREATED_SUCCESSFULLY);
        } catch (QueryException $ex) {
            $request->session()->flash('error', "Error ocurred: {$ex->getMessage()}");
            return back()->withInput();
        }

        return redirect()->route('books.show', [ 'id' => $book->id ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::with('author')->find($id);

        if ($book == NULL) {
            return redirect()->route('books.index')
                             ->with('error', Book::NOT_FOUND);
        }

        return view('books/show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->showForm($id, 'update');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request
            ->validate([
                'author_id' => 'required',
                'title'     => 'required',
            ]);
        $book = Book::find($id);

        if ($book == NULL) {
            return redirect()->route('books.index')
                             ->with('error', Book::NOT_FOUND);
        }

        try {
            $book->update($data);
            $request->session()->flash('status', Book::UPDATED_SUCCESSFULLY);
        } catch (QueryException $ex) {
            $request->session()->flash('error', "Error ocurred: {$ex->getMessage()}");
            return back()->withInput();
        }

        return redirect()->route('books.show', [ 'id' => $book->id ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);

        if ($book == NULL) {
            return redirect()->route('books.index')
                             ->with('error', Book::NOT_FOUND);
        }

        try {
            $book->delete();
        } catch (QueryException $ex) {
            return redirect()->route('books.index')
                ->with('error', "There was an error deleting the book: {$ex->getMessage()}");
        }

        return redirect()->route('books.index')
                         ->with('status', Book::DELETED_SUCCESSFULLY);
    }

    /**
     * Display Book form to user.
     *
     * @param  int $id
     * @param  string $action
     * @return \Illuminate\Http\Response
     */
    private function showForm($id = NULL, $action)
    {
        $author_id = (int) request()->query('author_id');

        if ($id != NULL) {
            $book = Book::find($id);

            if ($book == NULL) {
                return redirect()->route('books.index')
                                 ->with('error', Book::NOT_FOUND);
            }
            $author_id = $book->author_id;
        } else {
            $book = new Book;
        }

        $authors = Author::select([ 'id', 'name' ])->get();

        return view('books/form', compact('book', 'action', 'authors', 'author_id'));
    }
}
